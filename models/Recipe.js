var keystone = require('keystone');
var Types = keystone.Field.Types;

var Recipe = new keystone.List('Recipe');

var myStorage = new keystone.Storage({
	adapter: keystone.Storage.Adapters.FS,
	fs: {
		path: keystone.expandPath('./public/recipe-images'),
		publicPath: '/recipe-images/',
	},
	schema: {
		url: true,
	},
});

Recipe.add({
	title: {
		type: Types.Text,
		initial: true,
		required: true,
	},
	image: {
		type: Types.File,
		storage: myStorage,
		thumb: true,
	},
	description: {
		brief: { type: Types.Html, wysiwyg: true, height: 150 },
		extended: { type: Types.Html, wysiwyg: true, height: 400 },
	},
});

Recipe.defaultColumns = 'title, image|20%, description|20%, publishedDate|20%';
Recipe.register();
