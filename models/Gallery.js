var keystone = require('keystone');
var Types = keystone.Field.Types;

var myStorage = new keystone.Storage({
	adapter: keystone.Storage.Adapters.FS,
	fs: {
		path: keystone.expandPath('./public/recipe-images'),
		publicPath: '/recipe-images/',
	},
	schema: {
		url: true,
	},
});

/**
 * Gallery Model
 * =============
 */

var Gallery = new keystone.List('Gallery', {
	autokey: { from: 'name', path: 'key', unique: true },
});

Gallery.add({
	name: { type: String, required: true },
	publishedDate: { type: Date, default: Date.now },
	heroImage: {
		type: Types.File,
		storage: myStorage,
		thumb: true,
	},
	images: {
		type: Types.File,
		storage: myStorage,
		thumb: true,
	},
});

Gallery.register();
