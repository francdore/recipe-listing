import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'filter'
})

export class FilterPipe implements PipeTransform {
    transform(items: any[], searchText: string): any {
        return items.filter(item => item.title.toLowerCase().indexOf(searchText.toLowerCase()) !== -1);
    }
}