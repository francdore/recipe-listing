import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-recipe-listing',
  templateUrl: './recipe-listing.component.html',
  styleUrls: ['./recipe-listing.component.css']
})
export class RecipeListingComponent implements OnInit {
  public pageTitle: string = 'Our Recipes';
  searchText = '';
  recipes = [];

  constructor(private httpClient: HttpClient) {

  }

  ngOnInit() {
      this.httpClient.get('http://localhost:3000/api/recipes')
      .subscribe(
        (data: any[]) => {
          this.recipes = data;
          console.log(this.recipes);
        }
      )
  }

}
