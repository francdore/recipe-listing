import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

    contactForm: FormGroup;

    constructor(private fb: FormBuilder) {
        this.contactForm = this.fb.group({
            name: ['', Validators.required ],
            email: ['', Validators.required ],
            contact: ['', Validators.required ],
            message: ['']
        });
    }

    submitForm(value: any) {
        if (this.contactForm.invalid) {
            this.contactForm.get('name').markAsTouched();
            this.contactForm.get('email').markAsTouched();
            this.contactForm.get('contact').markAsTouched();
            return;
        }
        console.log(this.contactForm.value);
    }

    ngOnInit() {
    }

}
