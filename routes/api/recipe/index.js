var keystone = require('keystone');

var Recipe = keystone.list('Recipe').model;

var handlers = {
	getRecipes: function (req, res) {
		Recipe.find().exec(function (err, data) {
			if (err) {
				console.log(err);
				res.status(500).send('DB Error');
			}
			res.status(200).send(data);
		});
	},
};

module.exports = handlers;
